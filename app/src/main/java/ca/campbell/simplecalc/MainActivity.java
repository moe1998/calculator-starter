package ca.campbell.simplecalc;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

//  TODO: add a field to input a 2nd number, get the input and use it in calculations DONE
//  TODO: the inputType attribute forces a number keyboard, don't use it on the second field so you can see the difference DONE

//  TODO: add buttons & methods for subtract, multiply, divide DONE

//  TODO: add input validation: no divide by zero DONE
//  TODO: input validation: set text to show error when it occurs DONE

//  TODO: add a clear button that will clear the result & input fields DONE

//  TODO: the hint for the result widget is hard coded, put it in the strings file DONE

public class MainActivity extends Activity {
    EditText etNumber1;
    EditText etNumber2;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // get a handle on the text fields
        etNumber1 = (EditText) findViewById(R.id.num1);
        etNumber2 = (EditText) findViewById(R.id.num2);
        result = (TextView) findViewById(R.id.result);
    }  //onCreate()

    // TODO: replace with code that adds the two input numbers
    public void addNums(View v) {
        if(etNumber1.getText().toString().trim().equals("") || etNumber2.getText().toString().trim().equals("")) {
            result.setText(R.string.missing_param);
        }else {
            try {
                double num1 = Double.parseDouble(etNumber1.getText().toString());
                double num2 = Double.parseDouble(etNumber2.getText().toString());
                result.setText(Double.toString(num2 + num1));
            }catch (Exception ParseException){
                result.setText(R.string.only_num);
            }
        }
    }  //addNums()

    public void subNums(View v){
        if(etNumber1.getText().toString().trim().equals("") || etNumber2.getText().toString().trim().equals("")){
            result.setText(R.string.missing_param);
        }else {
            try {
                double num1 = Double.parseDouble(etNumber1.getText().toString());
                double num2 = Double.parseDouble(etNumber2.getText().toString());
                result.setText(Double.toString(num1 - num2));
            }catch(Exception ParseException){
                result.setText(R.string.only_num);
            }
        }
    }

    public void divNums(View v) {
        if (etNumber1.getText().toString().trim().equals("") || etNumber2.getText().toString().trim().equals("")) {
            result.setText(R.string.missing_param);
        } else {
            try {
                double num1 = Double.parseDouble(etNumber1.getText().toString());
                double num2 = Double.parseDouble(etNumber2.getText().toString());
                if (num2 == 0)
                    result.setText(R.string.zero_div);
                else
                    result.setText(Double.toString(num1 / num2));
            }catch(Exception ParseException){
                result.setText(R.string.only_num);
            }
        }
    }

    public void mulNums(View v){
        if (etNumber1.getText().toString().trim().equals("") || etNumber2.getText().toString().trim().equals("")) {
            result.setText(R.string.missing_param);
        }else{
            try {
                double num1 = Double.parseDouble(etNumber1.getText().toString());
                double num2 = Double.parseDouble(etNumber2.getText().toString());
                result.setText(Double.toString(num1 * num2));
            }catch(Exception ParseException){
                result.setText(R.string.only_num);
            }
        }
    }

    public void clear(View v){
        etNumber1.setText("");
        etNumber2.setText("");
        result.setText("");
    }
}